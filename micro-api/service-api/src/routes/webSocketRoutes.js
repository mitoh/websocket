'use strict';
module.exports = function(app) {
    var webSocketController = require('../controllers/webSocketController');
    
    app.route('/webSocket')
        .get(webSocketController.webSocketGet)
        .post(webSocketController.webSocketPost);
};